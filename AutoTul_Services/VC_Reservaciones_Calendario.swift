//
//  VC_ReservacionesCalendario.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 6/4/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_ReservacionesCalendario: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        //COLOCAR VC's AL INICIO DE LA CARGA
        if let primer_vc = arreglo_view_controllers.first{
            setViewControllers([primer_vc], direction: .forward, animated: true, completion: nil)
        }
    }

    //FUNCION PARA INSTANCIAR UN view controller
    func InstanciarVC (nombre_vc: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: nombre_vc)
    }
    
    
    //CREAR ARREGLO DE INSTANCIAS DE view controllers
    lazy var arreglo_view_controllers : [UIViewController] = [self.InstanciarVC(nombre_vc: "dia"),
                                                            self.InstanciarVC(nombre_vc: "mes")]
    
    //DEVOLVER SIGUIENTE
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index_view_controller = arreglo_view_controllers.index(of: viewController) else {return nil}
            let index_siguiente = index_view_controller + 1
        guard index_siguiente < arreglo_view_controllers.count else {return nil}
            return arreglo_view_controllers[index_siguiente]
    }
    
    //DEVOLVER ANTERIOR
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index_view_controller = arreglo_view_controllers.index(of: viewController) else {return nil}
            let index_previo = index_view_controller - 1
        guard index_previo >= 0 else {return nil}
            return arreglo_view_controllers[index_previo]
    }
    
}
