//
//  VC_bitacora_Detalle.swift
//  AutoTul_Services
//
//  Created by Ric on 6/7/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_bitacora_Detalle: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tabla_bitacora: UITableView!
    @IBOutlet weak var label_nombre_vehiculo: UILabel!
    
    var nombre_vehiculo = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabla_bitacora.delegate = self
        tabla_bitacora.dataSource = self
        
        label_nombre_vehiculo.text = nombre_vehiculo
    }
    
    @IBAction func PulsarCancelar(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tabla_bitacora.dequeueReusableCell(withIdentifier: "celda_obscura", for: indexPath)
        if indexPath.row % 2 == 0 {
            celda.contentView.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0)
        } else {
            celda.contentView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        }
        return celda
    }

}
