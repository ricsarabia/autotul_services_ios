//
//  VC_Registrarse.swift
//  AutoTul_Services
//
//  Created by Ricardo on 5/30/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_RegistroTaller: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var configuracion = false
    var ubicacion: String = "Ubicación del taller"
    let mecanicos = [["Juan Perez", #imageLiteral(resourceName: "icono_mecanico2")], ["Antonio Aguilar", #imageLiteral(resourceName: "icono_mecanico2")],["Hector Morales", #imageLiteral(resourceName: "icono_mecanico2")],["Ricardo Perez", #imageLiteral(resourceName: "icono_mecanico2")]]
    let especialidades = [["Afinación", #imageLiteral(resourceName: "icono_editar")], ["Balanceo", #imageLiteral(resourceName: "icono_editar")], ["Balanceo", #imageLiteral(resourceName: "icono_editar")], ["Balanceo", #imageLiteral(resourceName: "icono_editar")]]
    
    @IBOutlet weak var titulo_encabezado: UILabel!
    @IBOutlet weak var label_numero_vehiculos: UILabel!
    @IBOutlet weak var boton_ubicacion: UIButton!
    @IBOutlet weak var boton_registrar: UIButton!
    @IBOutlet weak var icono_encabezado: UIImageView!
    @IBOutlet weak var boton_menu: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        if configuracion{
            titulo_encabezado.text = "Configuracion Servicio"
            boton_registrar.setTitle("Actualizar", for: .normal)
            icono_encabezado.image = UIImage(named: "icono_rojo_configuracion")
            
            guard self.revealViewController() != nil else {return}
            boton_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        boton_ubicacion.setTitle(ubicacion, for: .normal)

    }
    
    @IBAction func PulsarCancelar(_ sender: Any) {
        guard !configuracion else { return }
        self.dismiss(animated: true, completion: nil)
    }
  
    var numero_vehiculos: Int8 = 0
    @IBAction func PulsarMas(_ sender: Any) {
        numero_vehiculos += 1
        label_numero_vehiculos.text = String.init(numero_vehiculos)
    }
    
    @IBAction func PulsarMenos(_ sender: Any) {
        guard numero_vehiculos > 0 else {return}
        numero_vehiculos -= 1
        label_numero_vehiculos.text = String.init(numero_vehiculos)
    }
    
    // MARK: Popover Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "TallerAPopoverEspecialidad" || segue.identifier == "TallerAPopoverMecanico") {
            let dvc = segue.destination as! VC_Popover
            dvc.nc = self.navigationController!
            let ppc = dvc.popoverPresentationController!
            ppc.permittedArrowDirections = .up
            let viewForSource = sender as! UIButton
            ppc.sourceView = viewForSource
            ppc.sourceRect = viewForSource.bounds
            ppc.delegate = self
            
            if (segue.identifier == "TallerAPopoverEspecialidad") {
                dvc.datasource = especialidades
                dvc.vcIdentifier = "vc_nuevaespecialidad"
            } else  {
                dvc.datasource = mecanicos
                dvc.vcIdentifier = "vc_nuevomecanico"
            }
        }
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

// EXTENSION PARA OCULTAR EL TECLADO CON TAP FUERA DEL MISMO, SE DEBE DE LLAMAR self.hideKeyboard() EN EL viewDidLoad() del UIViewController deseado
extension VC_RegistroTaller
{
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func dismissKeyboard(){
        view.endEditing(true)
    }
}
