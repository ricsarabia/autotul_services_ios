//
//  VC_Estatus.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Estatus: UIViewController, UITableViewDelegate, UITableViewDataSource{

    override func viewDidLoad() {
        super.viewDidLoad()

        tabla_estatus.delegate = self
        tabla_estatus.dataSource = self
        
        guard self.revealViewController() != nil else {return}
        botonMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

    }
    
    //OUTLETS
    @IBOutlet weak var botonMenu: UIButton!
    @IBOutlet weak var tabla_estatus: UITableView!
    
    //DELEGADOS TABLA
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identificador = ""
        if indexPath.row % 2 == 0 {
            identificador = "celda_obscura"
        }else{
            identificador = "celda_clara"
        }
        let celda = tabla_estatus.dequeueReusableCell(withIdentifier: identificador)!
        return celda
    }

}
