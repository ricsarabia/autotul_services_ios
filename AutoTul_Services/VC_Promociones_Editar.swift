//
//  VC_Promociones_Editar.swift
//  AutoTul_Services
//
//  Created by Ric on 6/6/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Promociones_Editar: UIViewController {
    
    var numero_promocion = 0
    var editar = false //POR DEFAULT ESTA VISTA ES PARA AGREGAR
    
    //OUTLETS
    @IBOutlet weak var label_numero: UILabel!
    @IBOutlet weak var campo_nombre: UITextField!
    @IBOutlet weak var boton_guardar: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if editar{
            boton_guardar.setTitle("Actualizar", for: .normal)
        }
        label_numero.text = String.init(numero_promocion + 1)
    }

    @IBAction func PulsarCancelar(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func PulsarGuardar(_ sender: UIButton) {
        let vista_promociones = storyboard?.instantiateViewController(withIdentifier: "Promociones") as! VC_Promociones
        
        if editar {
            guard numero_promocion < vista_promociones.promociones.count else {return}
            vista_promociones.promociones[numero_promocion] = campo_nombre.text!
            vista_promociones.fechas[numero_promocion] = "06/06/2016"
        } else {
            vista_promociones.promociones.append(campo_nombre.text!)
            vista_promociones.fechas.append("06/06/2016")
        }
        self.navigationController?.pushViewController(vista_promociones, animated: true)
    }
    

}
