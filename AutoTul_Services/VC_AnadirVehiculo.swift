//
//  VC_AnadirVehiculo.swift
//  AutoTul_Services
//
//  Created by Ric on 6/8/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_AnadirVehiculo: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()

        // Do any additional setup after loading the view.
    }

    @IBAction func PulsarAtras(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func PulsarGuardar(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
}



// EXTENSION PARA OCULTAR EL TECLADO CON TAP FUERA DEL MISMO, SE DEBE DE LLAMAR self.hideKeyboard() EN EL viewDidLoad() del UIViewController deseado
extension VC_AnadirVehiculo
{
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard(){
        view.endEditing(true)
    }
}
