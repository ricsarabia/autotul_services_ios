//
//  VC_Home.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/28/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Home: UIViewController{

    var estatus = false
    var recepcion = false
    var bitacora = false
    var reservaciones = false
    var pagos = false
    var estadisticas = false
    var configuracion = false
    var qr = false
    var promociones = false
    var perfil = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Ocultar_Containers()
        container_estatus.isHidden = false
        if recepcion { container_recepcion.isHidden = false }
        if bitacora { container_bitacora.isHidden = false }
        if reservaciones {container_reservaciones.isHidden = false }
        if pagos {container_pagos.isHidden = false }
        if estadisticas { container_estadisticas.isHidden = false }
        if configuracion {
            container_configuracion.isHidden = false

            }
        if qr { container_qr.isHidden = false }
        if promociones { container_promociones.isHidden = false }
        if perfil { container_perfil.isHidden = false }
    }
    
    // OUTLETS
    @IBOutlet weak var container_estatus: UIView!
    @IBOutlet weak var container_recepcion: UIView!
    @IBOutlet weak var container_bitacora: UIView!
    @IBOutlet weak var container_reservaciones: UIView!
    @IBOutlet weak var container_pagos: UIView!
    @IBOutlet weak var container_estadisticas: UIView!
    @IBOutlet weak var container_configuracion: UIView!
    @IBOutlet weak var container_qr: UIView!
    @IBOutlet weak var container_promociones: UIView!
    @IBOutlet weak var container_perfil: UIView!


    
    func Ocultar_Containers(){

        container_recepcion.isHidden = true
        container_bitacora.isHidden = true
        container_estatus.isHidden = true
        container_reservaciones.isHidden = true
        container_pagos.isHidden = true
        container_estadisticas.isHidden = true
        container_configuracion.isHidden = true
        container_qr.isHidden = true
        container_promociones.isHidden = true
        container_perfil.isHidden = true
    }
    
    //ACTIONS
    @IBAction func pulsarRecepcion(_ sender: Any) {
        Ocultar_Containers()
        container_recepcion.isHidden = false
    }

    @IBAction func pulsarBitacora(_ sender: Any) {
        Ocultar_Containers()
        container_bitacora.isHidden = false
    }
    
    @IBAction func pulsarEstatus(_ sender: Any) {
        Ocultar_Containers()
        container_estatus.isHidden = false
    }
    
    @IBAction func pulsarReservaciones(_ sender: Any) {
        Ocultar_Containers()
        container_reservaciones.isHidden = false
    }
    
    @IBAction func pulsarPagos(_ sender: Any) {
        Ocultar_Containers()
        container_pagos.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        //GUARDAR EL IDENTIFICADOR DEL SEGUE ACTIVADO
        let identificador = segue.identifier
        
        //INSTANCIAR EL ViewControler DESTINO COMO UINavigationController PARA ACCEDER A SUS PROPIEDADES
        let siguiente_escena = segue.destination as! UINavigationController
  
        //ASIRNAR true A LA PROPIEDAD EN VC_RegistroTaller CORRESPONDIENTE AL IDENTIFICADOR GUARDADO
        if identificador == "segue_configuracion"{
            let target = siguiente_escena.topViewController as! VC_RegistroTaller
            target.configuracion = true
        }
        
        
    }
    
    
}


// EXTENSION PARA OCULTAR EL TECLADO CON TAP FUERA DEL MISMO, SE DEBE DE LLAMAR self.hideKeyboard() EN EL viewDidLoad() del UIViewController deseado
//extension UIViewController
//{
//    func hideKeyboard(){
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
//            target: self,
//            action: #selector(UIViewController.dismissKeyboard))
//        view.addGestureRecognizer(tap)
//    }
//    
//    func dismissKeyboard(){
//        view.endEditing(true)
//    }
//}
