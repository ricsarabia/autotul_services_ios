//
//  VC_RecepcionSinCita.swift
//  AutoTul_Services
//
//  Created by Ric on 6/6/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_RecepcionSinCita: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //OUTLETS
    @IBOutlet weak var boton_menu: UIButton!
    @IBOutlet weak var tabla_clientes: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabla_clientes.delegate = self
        tabla_clientes.dataSource = self
        
        //acction Y gesture PARA MOSTRAR MENÚ
        guard self.revealViewController() != nil else {return}
        boton_menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var idenfiticador = ""
        if indexPath.row % 2 == 0 {
            idenfiticador = "celda_obscura"
        } else {
            idenfiticador = "celda_clara"
        }
        let celda = tabla_clientes.dequeueReusableCell(withIdentifier: idenfiticador, for: indexPath)
        return celda
    }
}
