//
//  VC_Pagos.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Pagos: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //OUTLETS
    @IBOutlet weak var tabla_pagos: UITableView!
    @IBOutlet weak var botonMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        guard self.revealViewController() != nil else {return}
            botonMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        tabla_pagos.delegate = self
        tabla_pagos.dataSource = self
    }
    
    //TABLE DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identificador = ""
        if indexPath.row % 2 == 0
        { identificador = "celda_clara" }
        else
        { identificador = "celda_obscura" }
        
        let celda = tabla_pagos.dequeueReusableCell(withIdentifier: identificador, for: indexPath)
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PagosADetalle", sender: self)
    }

}
