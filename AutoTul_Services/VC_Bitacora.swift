//
//  VC_Bitacora.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Bitacora: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //OUTLETS
    @IBOutlet weak var botonMenu: UIButton!
    @IBOutlet weak var tabla_bitacora: UITableView!
    var nombres = ["Ximena","Manolo","Diana","Andrea"]
    var autos = ["Seat Ibiza 2015","Ford Mustang 2018","Nissan Sentra 2014","VW Beetlee 2018"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabla_bitacora.delegate = self
        tabla_bitacora.dataSource = self
        guard self.revealViewController() != nil else {return}
        botonMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    
    //--PROTOCOLOS TABLA
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nombres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0 {
            
        }else{
            
        }
        let celda = tabla_bitacora.dequeueReusableCell(withIdentifier: "celda_obscura", for: indexPath) as! VC_Bitacora_Cell
        if indexPath.row % 2 == 0{
            celda.contentView.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0) //fondo obscuro
        } else {
            celda.contentView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0) //fondo claro
        }
        
        celda.label_nombre.text = nombres[indexPath.row]
        celda.label_auto.text = autos[indexPath.row]
        return celda
    }
    //--
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "mostrar_detalle_bitacora" else {return}
            let siguiente_escena = segue.destination as! VC_bitacora_Detalle
            let index = tabla_bitacora.indexPathForSelectedRow!
            siguiente_escena.nombre_vehiculo = autos[index.row]
    }
}
