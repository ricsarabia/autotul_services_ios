//
//  BarraNavegacionPersonalizada.swift
//  AutoTul_Services
//
//  Created by Ricardo on 5/30/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class BarraNavegacionPersonalizada: UINavigationBar {
    
    override func draw(_ rect: CGRect) {

        //CONFIGURAR EL COLOR
        self.isTranslucent = false
        self.tintColor = UIColor.white
        self.backgroundColor = UIColor(red:0.66, green:0.12, blue:0.14, alpha:1.0)

        //CONFIGURAR EL ESTILO DE LOS TITULOS
        let navbarFont = UIFont(name: "Roboto", size: 17) ?? UIFont.systemFont(ofSize: 17)
        self.titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName: UIColor.white]


        //CONFIGURAR LA ALTURA CORRECTA
        let viejoframe = self.bounds //guardamos el tamaño previo
        let nuevoFrame = CGRect(x: 0, y: 0, width: viejoframe.width, height: 53) //el height original era 44
        self.frame = nuevoFrame

    }

    
}
