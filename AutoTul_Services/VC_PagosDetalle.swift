//
//  VC_PagosDetalle.swift
//  AutoTul_Services
//
//  Created by sandra guzman on 08/06/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_PagosDetalle: UIViewController, UIPopoverPresentationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func PulsarAtras(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: Popover Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dvc = segue.destination
        let ppc = dvc.popoverPresentationController!
        ppc.permittedArrowDirections = .up
        let viewForSource = sender as! UIButton
        ppc.sourceView = viewForSource
        ppc.sourceRect = viewForSource.bounds
        ppc.delegate = self
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @IBAction func sendDetailsPaymnets(_ sender: Any) {
        let alert = UIAlertController(title: "Detalle de pago", message: "El detalle de pago a sido enviado", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
