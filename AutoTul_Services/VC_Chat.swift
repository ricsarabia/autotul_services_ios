//
//  VC_Chat.swift
//  AutoTul_Services
//
//  Created by Ric on 6/7/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Chat: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    //OUTLETS
    
    @IBOutlet weak var tabla_chat: UITableView!
    @IBOutlet weak var campo_mensaje: UITextField!
    var mensajes = ["¡Hola! ¿Tienen servicio de hojalatería?",
                    "Lo sentimos pero no contamos con ese servicio."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        tabla_chat.delegate = self
        tabla_chat.dataSource = self
        campo_mensaje.delegate = self
    }

    @IBAction func PulsarAtras(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mensajes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identificador = ""
        if indexPath.row % 2 == 0{
            identificador = "celda_izquierda"
        } else {
            identificador = "celda_derecha"
        }
        
        let celda = tabla_chat.dequeueReusableCell(withIdentifier: identificador, for: indexPath) as! VC_Chat_Celda
        celda.label_mensaje.text = mensajes[indexPath.row]
        return celda
    }
    
    
    
    @IBAction func PulsarEnviar(_ sender: Any) {
        self.EnviarMensaje()
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    
    
    func EnviarMensaje(){
        guard campo_mensaje.text != "" else {return}
        mensajes.append(campo_mensaje.text!)
        tabla_chat.reloadData()
        campo_mensaje.text = ""
        let index_ultimo = IndexPath(row: mensajes.count - 1, section: 0) //tambien las secciones empiezan en 0 😒
        tabla_chat.scrollToRow(at: index_ultimo, at: .bottom, animated: true)
    }
    

}




// EXTENSION PARA OCULTAR EL TECLADO CON TAP FUERA DEL MISMO, SE DEBE DE LLAMAR self.hideKeyboard() EN EL viewDidLoad() del UIViewController deseado
extension VC_Chat
{
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func dismissKeyboard(){
        view.endEditing(true)
    }
}
