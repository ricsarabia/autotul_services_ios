//
//  VC_Configuracion.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Configuracion: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard self.revealViewController() != nil else {return}
        botonMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    @IBOutlet weak var botonMenu: UIButton!
    

}
