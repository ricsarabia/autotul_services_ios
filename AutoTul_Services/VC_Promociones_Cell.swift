//
//  VC_Promociones_Cell.swift
//  AutoTul_Services
//
//  Created by Ric on 6/6/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Promociones_Cell: UITableViewCell {

    //OUTLETS
    @IBOutlet weak var label_numero: UILabel!
    @IBOutlet weak var label_promocion: UILabel!
    @IBOutlet weak var label_fecha: UILabel!
    @IBOutlet weak var boton_editar: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
