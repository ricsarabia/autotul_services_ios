//
//  VC_Reservaciones_Dia.swift
//  AutoTul_Services
//
//  Created by Ricardo on 6/5/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Reservaciones_Dia: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //OUTLETS
    @IBOutlet weak var tabla_dia: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabla_dia.delegate = self
        tabla_dia.dataSource = self

    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identificador = ""
        if indexPath.row % 2 == 0
            { identificador = "celda_clara" }
        else
            { identificador = "celda_obscura" }
        
        let cell = tabla_dia.dequeueReusableCell(withIdentifier: identificador, for: indexPath)

        return cell
    }

}
