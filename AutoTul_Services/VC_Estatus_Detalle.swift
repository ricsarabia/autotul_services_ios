//
//  VC_Estatus_Detalle.swift
//  AutoTul_Services
//
//  Created by Ric on 6/7/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Estatus_Detalle: UIViewController, UIPopoverPresentationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func PulsarAtras(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    // MARK: Popover Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "MostrarPopupDetalle") {
            let dvc = segue.destination
            let ppc = dvc.popoverPresentationController!
            ppc.permittedArrowDirections = .right
            let viewForSource = sender as! UIButton
            ppc.sourceView = viewForSource
            ppc.sourceRect = viewForSource.bounds
            ppc.delegate = self
        }
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @IBAction func showPopup(_ sender: Any) {
        let alert = UIAlertController(title: "Estatus detalle", message: "El estatus detalle se a entregado", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true) { 
            self.navigationController?.popViewController(animated: true)
        }
    }
}
