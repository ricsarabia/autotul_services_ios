//
//  VC_UbicacionTaller.swift
//  AutoTul_Services
//
//  Created by Ricardo on 6/1/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit
import MapKit

class VC_UbicacionTaller: UIViewController, MKMapViewDelegate, UITextFieldDelegate {
    
    //--
    override func viewDidLoad() {
        super.viewDidLoad()
        
        campo_direccion_taller.delegate = self
        
        //CONFIGURAR MAPA
        vista_mapa.delegate = self
        vista_mapa.showsUserLocation = true
       
        //MOSTRAR UBICACIÓN INICIAL EN MAPA
        let geo_coder = CLGeocoder()
        geo_coder.geocodeAddressString("Hamburgo 70, Juarez", completionHandler: { placemarks, error in
            
            if let error = error {
                print(error)
                return
            }
            guard let mis_placemarks = placemarks else {return}
                let un_placemark = mis_placemarks[0]
                let anotacion = MKPointAnnotation()
                anotacion.title = "Su Taller"
                anotacion.subtitle = "Cuautemoc, CDMX"
            guard let ubicacion = un_placemark.location else {return}
                anotacion.coordinate = ubicacion.coordinate
                self.vista_mapa.showAnnotations([anotacion], animated: true)
                self.vista_mapa.selectAnnotation(anotacion, animated: true)
        })
    }
    //--

    
    //OUTLETS
    @IBOutlet weak var vista_mapa: MKMapView!
    @IBOutlet weak var campo_direccion_taller: UITextField!
    
    //ACTIONS
    @IBAction func PulsarCancelar(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func PulsarGuardar(_ sender: Any) {
        
        //INSTANCIAR PARA ACCEDER A LAS PROPIEDADES DE VC_RegistroTaller
        let vista_registro_taller = storyboard?.instantiateViewController(withIdentifier: "RegistroTaller") as! VC_RegistroTaller
        
        //GUARDAR VALOR DEL CAMPO DE BÚSQUEDA EN LA VARIABLE ubicacion DE VC_RegistroTaller
        vista_registro_taller.ubicacion = campo_direccion_taller.text!
        
        //IR A RegistroTaller
        self.navigationController?.pushViewController(vista_registro_taller, animated: true)
    }
    
    
    //BUSCAR EN EL MAPA AL PULSAR ENTER EN EL TECLADO
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //CERRAR TECLADO
        self.view.endEditing(true)
        
        //MOSTRAR EN MAPA UBICACIÓN INGRESADA POR USUARIO
        let geo_coder = CLGeocoder()
        geo_coder.geocodeAddressString(campo_direccion_taller.text!, completionHandler: { placemarks, error in
            //--inicia closure
            if let error = error {
                print(error)
                return
            }
            guard let mis_placemarks = placemarks else {return}
            let un_placemark = mis_placemarks[0]
            let anotacion = MKPointAnnotation()
            anotacion.title = "Su Taller"
            anotacion.subtitle = self.campo_direccion_taller.text!
            guard let ubicacion = un_placemark.location else {return}
            anotacion.coordinate = ubicacion.coordinate
            self.vista_mapa.showAnnotations([anotacion], animated: true)
            self.vista_mapa.selectAnnotation(anotacion, animated: true)
            //--termina closure
        })

        return true
    }
    
    
}
