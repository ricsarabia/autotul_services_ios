//
//  VC_Reservaciones_Mes.swift
//  AutoTul_Services
//
//  Created by Ric on 6/8/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Reservaciones_Mes: UIViewController {
    
    @IBOutlet weak var label_año: UILabel!
    var año = 2016
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label_año.text = String.init(año)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func PulsarDisminuir(_ sender: UIButton) {
        año -= 1
        label_año.text = String.init(año)
    }

    @IBAction func PulsarAumentar(_ sender: UIButton) {
        año += 1
        label_año.text = String.init(año)
    }
    


}
