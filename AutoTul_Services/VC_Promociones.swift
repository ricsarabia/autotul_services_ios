//
//  VC_Promociones.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Promociones: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var promociones = ["Lavado de motor $500.00",
                       "Cupón por $250,00",
                       "Lavado de motor 2 x 1!!"]
    var fechas = ["01/abril/2017",
                  "04/abril/2017",
                  "20/abril/2017"]
    
    //var ir_a_editar = false
    
    var index_para_enviar = 0
    
    
    //OUTLETS
    @IBOutlet weak var tabla_promociones: UITableView!
    @IBOutlet weak var botonMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabla_promociones.delegate = self
        tabla_promociones.dataSource = self
        
        //GESTO PARA MOSTRAR MENÚ
        guard self.revealViewController() != nil else {return}
        botonMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda = tabla_promociones.dequeueReusableCell(withIdentifier: "celda_obscura", for: indexPath) as! VC_Promociones_Cell
        
        if indexPath.row % 2 == 0 {
            celda.contentView.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0)
        } else {
            celda.contentView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        }
        
        celda.label_numero.text = String.init(indexPath.row + 1)
        celda.label_promocion.text = promociones[indexPath.row]
        celda.label_fecha.text = fechas[indexPath.row]
        celda.boton_editar.tag = indexPath.row
        celda.boton_editar.addTarget(self, action: #selector(self.PulsarBotonEditar), for: .touchUpInside)
        
        return celda
    }
    
    
    
    @IBAction func PulsarAgregar(_ sender: UIButton) {
        
        let siguiente_escena = storyboard?.instantiateViewController(withIdentifier: "Promociones_Editar") as! VC_Promociones_Editar
        siguiente_escena.numero_promocion = promociones.count
        navigationController?.pushViewController(siguiente_escena, animated: true)
        
    }
    
        
    
    func PulsarBotonEditar(sender: UIButton){
        
        let siguiente_escena = storyboard?.instantiateViewController(withIdentifier: "Promociones_Editar") as! VC_Promociones_Editar
        siguiente_escena.numero_promocion = sender.tag
        siguiente_escena.editar = true
        navigationController?.pushViewController(siguiente_escena, animated: true)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promociones.count
    }
    
    
    @IBAction func PulsarEnviar(_ sender: Any) {
        //MOSTRAR ALERTA
        let controladorAlerta = UIAlertController(title: "Listo", message: "Promociones publicadas exitosamente", preferredStyle: .alert)
        let accionDefault = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        controladorAlerta.addAction(accionDefault)
        present(controladorAlerta, animated: true, completion: nil)
    }
    
    @IBAction func PulsarBorrar(_ sender: UIButton) {
        //MOSTRAR ALERTA
        
        let controladorAlerta = UIAlertController(title: "Borrar", message: "¿Desea borrar toda la lista de promociones?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            self.promociones.removeAll()
            self.tabla_promociones.reloadData()
        })
        
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        controladorAlerta.addAction(ok)
        controladorAlerta.addAction(cancelar)
        present(controladorAlerta, animated: true, completion: nil)

    }
    
    
    
    
}
