//
//  VC_Bitacora_Cell.swift
//  AutoTul_Services
//
//  Created by Ric on 6/7/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Bitacora_Cell: UITableViewCell {
    
    @IBOutlet weak var label_nombre: UILabel!
    @IBOutlet weak var label_auto: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
