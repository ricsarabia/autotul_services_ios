//
//  VC_RecuperarContraseña.swift
//  AutoTul_Services
//
//  Created by sandra guzman on 08/06/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_RecuperarContraseña : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backScreen(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        let alert = UIAlertController(title: "Recuperar contraseña", message: "Ha sido enviado satisfactoriamente un correo para el cambio de contraseña", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
