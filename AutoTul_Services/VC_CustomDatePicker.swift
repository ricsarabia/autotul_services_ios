//
//  VC_CustomDatePicker.swift
//  AutoTul_Services
//
//  Created by sandra guzman on 06/06/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class CustomDatePicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    let time = ["7:00 a.m.", "8:00 a.m.", "9:00 a.m.", "10:00 a.m.", "11:00 a.m.", "12:00 p.m.", "1:00 p.m.", "2:00 p.m.", "3:00 p.m.", "4:00 p.m.", "5:00 p.m."]
    let GRAY = UIColor.init(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
    let LIGHT_GRAY = UIColor.init(red: 204/255, green: 204/255, blue: 204/255, alpha: 1)
    let RED = UIColor.init(red: 144/255, green: 35/255, blue: 39/255, alpha: 1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        self.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.frame = frame
        self.delegate = self
    }
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (component == 0 || component == 2) {
            return 1
        } else {
            return time.count
        }
    }
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return time[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 1
        label.minimumScaleFactor = 0.2
        label.adjustsFontSizeToFitWidth = true
        
        if (component == 0 || component == 2) {
            label.frame = CGRect(x: 0, y: 0, width: pickerView.frame.width/6.1, height: 30)
            label.backgroundColor = LIGHT_GRAY
            label.textColor = RED
            label.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
            label.text = (component == 0) ? "de" : "a"
        } else {
            
            label.frame = CGRect(x: 0, y: 0, width: pickerView.frame.width/3, height: 30)
            label.backgroundColor = (row == pickerView.selectedRow(inComponent: component)) ? LIGHT_GRAY : UIColor.white
            label.textColor = (row == pickerView.selectedRow(inComponent: component)) ? RED : GRAY
            label.font = (row == pickerView.selectedRow(inComponent: component)) ? UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold) : UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
            label.text = time[row]
        }
        
        return label
    }
}
