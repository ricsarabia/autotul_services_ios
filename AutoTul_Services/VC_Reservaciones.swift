//
//  VC_Reservaciones.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Reservaciones: UIViewController {
    
    var vista_calendario: VC_ReservacionesCalendario?
    @IBOutlet weak var botonMenu: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard self.revealViewController() != nil else {return}
        botonMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "segue_a_calendario" else {return}
        vista_calendario = segue.destination as? VC_ReservacionesCalendario
    }
    
    @IBAction func PulsarDiaOMes(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "dia")
            vista_calendario?.setViewControllers([vc!], direction: .reverse, animated: true, completion: nil)
        }
        if sender.selectedSegmentIndex == 1 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "mes")
            vista_calendario?.setViewControllers([vc!], direction: .forward, animated: true, completion: nil)
        }
    }
    
    //FUNCION PARA INSTANCIAR UN view controller
    func InstanciarVC (nombre_vc: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: nombre_vc)
    }

}
