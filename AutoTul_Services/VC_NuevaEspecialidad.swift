//
//  VC_NuevaEspecialidad.swift
//  AutoTul_Services
//
//  Created by Ricardo on 5/31/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_NuevaEspecialidad: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func PulsarAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func PulsarRegistrar(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
