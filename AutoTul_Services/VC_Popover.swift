//
//  VC_Popover.swift
//  AutoTul_Services
//
//  Created by sandra guzman on 07/06/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Popover : UIViewController, UITableViewDataSource, UITableViewDelegate {
    var datasource = [[String(), UIImage()]]
    var nc: UINavigationController = UINavigationController()
    var vcIdentifier:String = ""
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var campo_servicio: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.superview?.layer.cornerRadius = 0
        self.view.superview?.layer.borderWidth = 1
        self.view.superview?.layer.borderColor = UIColor.gray.cgColor
        if vcIdentifier == "vc_nuevoservicio"{
            addBtn.setTitle("Otro servicio", for: .normal)
            campo_servicio.isHidden = false
        }
    }
    
    // MARK: TableView Datasource - Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = datasource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "vc_PopupCell1", for: indexPath) as! VC_PopupCell1
        cell.details.text = item[0] as? String
        cell.icon.image = item[1] as? UIImage
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 0.5))
        headerView.backgroundColor = UIColor.init(red: 105/255, green: 171/255, blue: 207/255, alpha: 1)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextVC(_ sender: Any) {
        self.dismiss(animated: true) { 
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard self.vcIdentifier != "vc_nuevoservicio" else {return}
            let controller = storyboard.instantiateViewController(withIdentifier: self.vcIdentifier)
            self.nc.pushViewController(controller, animated: true)
        }
    }
    
}

class VC_PopupCell1 : UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var details: UILabel!
}

