//
//  VC_Recepcion.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 5/22/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Recepcion: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var servicios = [["Diagnóstico",#imageLiteral(resourceName: "icono_registro")],
                     ["Alineación y Balanceo",#imageLiteral(resourceName: "icono_registro")],
                     ["Cambio de llantas",#imageLiteral(resourceName: "icono_registro")]]
    
    var mecanicos = [["Juan Perez", #imageLiteral(resourceName: "icono_mecanico2")],
                     ["Antonio Aguilar", #imageLiteral(resourceName: "icono_mecanico2")],
                     ["Hector Morales", #imageLiteral(resourceName: "icono_mecanico2")],
                     ["Ricardo Perez", #imageLiteral(resourceName: "icono_mecanico2")]]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()

    }

    @IBAction func PulsarAtras(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func PulsarGuardar(_ sender: UIButton) {
        //MOSTRAR ALERTA
        let controladorAlerta = UIAlertController(title: "Listo", message: "Cita guardada correctamente", preferredStyle: .alert)
        let accionDefault = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        controladorAlerta.addAction(accionDefault)
        present(controladorAlerta, animated: true, completion: nil)
    }

    
    // MARK: Popover Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "recepcion_a_servicios" || segue.identifier == "recepcion_a_mecanicos") {
            let dvc = segue.destination as! VC_Popover
            dvc.nc = self.navigationController!
            
            let ppc = dvc.popoverPresentationController!
            ppc.permittedArrowDirections = .up
            
            let viewForSource = sender as! UIButton
            ppc.sourceView = viewForSource
            ppc.sourceRect = viewForSource.bounds
            
            ppc.delegate = self
            
            if (segue.identifier == "recepcion_a_servicios") {
                dvc.datasource = servicios
                dvc.vcIdentifier = "vc_nuevoservicio"
            } else  {
                dvc.datasource = mecanicos
                dvc.vcIdentifier = "vc_nuevomecanico"
            }
        }
    }
    
    func adaptivePresentationStyle(for controller:UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    
    
}



// EXTENSION PARA OCULTAR EL TECLADO CON TAP FUERA DEL MISMO, SE DEBE DE LLAMAR self.hideKeyboard() EN EL viewDidLoad() del UIViewController deseado
extension VC_Recepcion
{
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard(){
        view.endEditing(true)
    }
}
