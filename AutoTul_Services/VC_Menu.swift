//
//  VC_Menu.swift
//  AutoTul_Services
//
//  Created by Ric Sarabia on 6/2/17.
//  Copyright © 2017 Onikom. All rights reserved.
//

import UIKit

class VC_Menu: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //AVISAR A VC_Home QUÉ SUBVIEW MOSTRAR DE ACUERDO AL BOTÓN PULSADO
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //GUARDAR (SI EXISTE) EL IDENTIFICADOR DEL SEGUE ACTIVADO
        guard let identificador = segue.identifier else {return}
        
        //INSTANCIAR (SI EXISTE) EL ViewControler DESTINO COMO VC_Home PARA ACCEDER A SUS PROPIEDADES
        guard let siguiente_escena = segue.destination as? VC_Home else {return}
        
        //ASIRNAR true A LA PROPIEDAD EN VC_Home CORRESPONDIENTE AL IDENTIFICADOR GUARDADO
        switch identificador {
        //case "SegueDesdeEstadisticas":
        //    siguiente_escena.estadisticas = true
        case "SegueDesdeConfiguracion":
            siguiente_escena.configuracion = true
        case "SegueDesdeQR":
            siguiente_escena.qr = true
        case "SegueDesdePromociones":
            siguiente_escena.promociones = true
        //case "SegueDesdePerfil":
        //    siguiente_escena.perfil = true
        default:
            return
        }
    }

}
